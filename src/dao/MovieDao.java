package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Movie;
import util.DbUtil;

public class MovieDao {

    private Connection connection;

    public MovieDao() {
        connection = DbUtil.getConnection();
    }

    public void addMovie(Movie pelicula) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into movies(title,director,year) values (?, ?, ?)");
            preparedStatement.setString(1, pelicula.getTitle());
            preparedStatement.setString(2, pelicula.getDirector());
            preparedStatement.setInt(3, pelicula.getYear());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteMovie(int id) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from movies where id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateMovie(Movie pelicula) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update movies set title=?, director=?, year=? " +
                            "where id=?");
            preparedStatement.setString(1, pelicula.getTitle());
            preparedStatement.setString(2, pelicula.getDirector());
            preparedStatement.setInt(3, pelicula.getYear());
            preparedStatement.setInt(4, pelicula.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Movie> getAllMovies() {
        List<Movie> movies = new ArrayList<Movie>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from movies");
            while (rs.next()) {
                Movie pelicula = new Movie();
                pelicula.setId(rs.getInt("id"));
                pelicula.setTitle(rs.getString("title"));
                pelicula.setDirector(rs.getString("director"));
                pelicula.setYear(rs.getInt("year"));
               
                movies.add(pelicula);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return movies;
    }

    public Movie getMovieById(int id) {
        Movie pelicula = new Movie();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from movies where id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                pelicula.setId(rs.getInt("id"));
                pelicula.setTitle(rs.getString("title"));
                pelicula.setDirector(rs.getString("director"));
                pelicula.setYear(rs.getInt("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pelicula;
    }
}
