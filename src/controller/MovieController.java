package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MovieDao;
import model.Movie;

public class MovieController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/movie.jsp";
    private static String LIST_MOVIE = "/listMovie.jsp";
    private MovieDao dao;

    public MovieController() {
        super();
        dao = new MovieDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            int id = Integer.parseInt(request.getParameter("id"));
            dao.deleteMovie(id);
            forward = LIST_MOVIE;
            request.setAttribute("movies", dao.getAllMovies());    
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            int id = Integer.parseInt(request.getParameter("id"));
            Movie pelicula = dao.getMovieById(id);
            request.setAttribute("movie", pelicula);
        } else if (action.equalsIgnoreCase("listMovie")){
            forward = LIST_MOVIE;
            request.setAttribute("movies", dao.getAllMovies());
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Movie pelicula = new Movie();
        pelicula.setTitle(request.getParameter("title"));
        pelicula.setDirector(request.getParameter("director"));
        pelicula.setYear(Integer.parseInt(request.getParameter("year")));

        String movieid = request.getParameter("id");
        if(movieid == null || movieid.isEmpty())
        {
            dao.addMovie(pelicula);
        }
        else
        {
            pelicula.setId(Integer.parseInt(movieid));
            dao.updateMovie(pelicula);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_MOVIE);
        request.setAttribute("movies", dao.getAllMovies());
        view.forward(request, response);
    }
}