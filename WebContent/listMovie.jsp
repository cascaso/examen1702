<%@ include file = "WEB-INF/view/header.jsp" %>
	<main>
    <table border=1>
        <thead>
            <tr>
            	<th>Id</th>
                <th>Title</th>
                <th>Director</th>
                <th>Year</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${movies}" var="movie">
                <tr>
                	<td><c:out value="${movie.id}" /></td>         	
                    <td><c:out value="${movie.title}" /></td>
                    <td><c:out value="${movie.director}" /></td>
                    <td><c:out value="${movie.year}" /></td>
                    <td><a href="MovieController?action=edit&id=<c:out value="${movie.id}"/>">Update</a></td>
                    <td><a href="MovieController?action=delete&id=<c:out value="${movie.id}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p><a href="MovieController?action=insert">Add Movie</a></p>
    
    </main>
<%@ include file = "WEB-INF/view/footer.jsp" %>